package com.padel.email.service;

import java.util.List;

import com.padel.email.model.LeaveMasterEntity;


public interface LeaveService {
	
	List<LeaveMasterEntity> findAll();
	
	List<LeaveMasterEntity> findByStaffId(String code);
	
	LeaveMasterEntity findByLeaveId(String code);
	
	List<LeaveMasterEntity> findLeaveNotApproved(String staffId);
	
	List<LeaveMasterEntity> getNewLeave(String staffId);
	
	double getTotalLeaveOfTheYear(String staffId, String year);
	
	double getTotalSickLeaveUseOfTheYear(String staffId, String year);
	
	List<LeaveMasterEntity> getAllLeaveExcludeYou(String staffId);
	
	List<LeaveMasterEntity> getLeaveWithStatus(String staffId, String status);
	
	List<LeaveMasterEntity> getUnapproveLeave();

}
