package com.padel.email.service;

import java.util.List;

import com.padel.email.model.CheckmasterEntity;
import com.padel.email.model.CoStaffEntity;

public interface CheckService {
	
	List<CoStaffEntity> getAllHQStaff();
	CheckmasterEntity getCheckInfo(String staffId, String date);
	String getStaffID(int id);
	void update(double temp, String status,String time, String date, String staffId);
	CheckmasterEntity create(CheckmasterEntity cm);
	String getMaxID();

}
