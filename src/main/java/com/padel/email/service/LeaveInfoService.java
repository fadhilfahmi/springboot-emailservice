package com.padel.email.service;

import com.padel.email.model.LeaveInfoEntity;

public interface LeaveInfoService {
	
	LeaveInfoEntity findByStaffId(String code);
	
	LeaveInfoEntity findByStaffIdAndYear(String code, String year);
	
	double getBalanceEligibleLeave(String code, String year);
	
	double getEligibleLeaveForTheMonth(String code, String year, String date);
	
}
