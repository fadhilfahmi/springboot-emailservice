package com.padel.email.service;

import java.util.Collection;
import java.util.List;

import com.padel.email.model.SuperviseInfoEntity;



public interface SuperviseService {

	List<String> getSuperviseeIDs(String code, int level);
	SuperviseInfoEntity getSuperViseInfo(String code);
	
	
}
