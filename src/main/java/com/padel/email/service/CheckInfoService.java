package com.padel.email.service;

import com.padel.email.model.CheckformEntity;

public interface CheckInfoService {

	CheckformEntity getCheckFormInfo(String checkId);
	CheckformEntity create(CheckformEntity cf);

}
