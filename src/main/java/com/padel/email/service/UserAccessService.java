package com.padel.email.service;

import com.padel.email.model.UserAccessEntity;

public interface UserAccessService {
	
	UserAccessEntity findByStaffId(String code);

}
