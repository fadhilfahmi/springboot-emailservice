package com.padel.email.service;

import java.util.List;

import com.padel.email.model.CoStaffEntity;
import com.padel.email.model.StaffModel;




public interface StaffService {
	
	List<CoStaffEntity> findAll();
	
	CoStaffEntity findByStaffid(String code);
	
	CoStaffEntity findByEmail(String code);
	
	StaffModel getStaffInfoWithUserAccess(String code);


}
