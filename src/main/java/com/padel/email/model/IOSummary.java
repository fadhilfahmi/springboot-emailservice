package com.padel.email.model;

import java.util.List;

import lombok.Data;

@Data
public class IOSummary {
	private List<IOData> io;
}
