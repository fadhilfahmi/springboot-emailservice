package com.padel.email.model;

import lombok.Data;

@Data
public class StaffModel {
	
	private int userLevel;
	private CoStaffEntity coStaff;

}
