package com.padel.email.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "checkmaster", schema = "eleave", catalog = "")
public class CheckmasterEntity {
    private String id;
    private String staffId;
    private String date;
    private String timescan;
    private String timevalid;
    private Double temperature;
    private String status;
    private String timeout;

    @Id
    @Column(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @Basic
    @Column(name = "date")
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Basic
    @Column(name = "timescan")
    public String getTimescan() {
        return timescan;
    }

    public void setTimescan(String timescan) {
        this.timescan = timescan;
    }

    @Basic
    @Column(name = "timevalid")
    public String getTimevalid() {
        return timevalid;
    }

    public void setTimevalid(String timevalid) {
        this.timevalid = timevalid;
    }

    @Basic
    @Column(name = "temperature")
    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "timeout")
    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckmasterEntity that = (CheckmasterEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(staffId, that.staffId) &&
                Objects.equals(date, that.date) &&
                Objects.equals(timescan, that.timescan) &&
                Objects.equals(timevalid, that.timevalid) &&
                Objects.equals(temperature, that.temperature) &&
                Objects.equals(status, that.status) &&
                Objects.equals(timeout, that.timeout);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, staffId, date, timescan, timevalid, temperature, status, timeout);
    }
}
