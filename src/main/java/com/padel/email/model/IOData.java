package com.padel.email.model;

import lombok.Data;

@Data
public class IOData {
	
	private String date;
	private String timein;
	private String timout;
	private String duration;
	private String colorduration;
	private String colortimein;
	private String colortimout;
	private boolean completeHour;
	private boolean lateIn;
	private boolean earlyOut;
}
