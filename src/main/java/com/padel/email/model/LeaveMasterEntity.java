package com.padel.email.model;

import javax.persistence.*;

@Entity
@Table(name = "leave_master", schema = "eleave", catalog = "")
public class LeaveMasterEntity {
    private String leaveId;
    private String staffId;
    private String type;
    private String reason;
    private Integer days;
    private String dateapply;
    private String datestart;
    private String dateend;
    private String supervisorId;
    private String supervisorDate;
    private String headId;
    private String headDate;
    private String hrId;
    private String hrDate;
    private String checkId;
    private String checkDate;
    private String status;
    private String year;
    private String period;
    private Integer stafflevel;

    @Id
    @Column(name = "leaveID")
    public String getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(String leaveId) {
        this.leaveId = leaveId;
    }

    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "reason")
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Basic
    @Column(name = "days")
    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    @Basic
    @Column(name = "dateapply")
    public String getDateapply() {
        return dateapply;
    }

    public void setDateapply(String dateapply) {
        this.dateapply = dateapply;
    }

    @Basic
    @Column(name = "datestart")
    public String getDatestart() {
        return datestart;
    }

    public void setDatestart(String datestart) {
        this.datestart = datestart;
    }

    @Basic
    @Column(name = "dateend")
    public String getDateend() {
        return dateend;
    }

    public void setDateend(String dateend) {
        this.dateend = dateend;
    }

    @Basic
    @Column(name = "supervisorID")
    public String getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }

    @Basic
    @Column(name = "supervisor_date")
    public String getSupervisorDate() {
        return supervisorDate;
    }

    public void setSupervisorDate(String supervisorDate) {
        this.supervisorDate = supervisorDate;
    }

    @Basic
    @Column(name = "headID")
    public String getHeadId() {
        return headId;
    }

    public void setHeadId(String headId) {
        this.headId = headId;
    }

    @Basic
    @Column(name = "head_date")
    public String getHeadDate() {
        return headDate;
    }

    public void setHeadDate(String headDate) {
        this.headDate = headDate;
    }

    @Basic
    @Column(name = "hrID")
    public String getHrId() {
        return hrId;
    }

    public void setHrId(String hrId) {
        this.hrId = hrId;
    }

    @Basic
    @Column(name = "hr_date")
    public String getHrDate() {
        return hrDate;
    }

    public void setHrDate(String hrDate) {
        this.hrDate = hrDate;
    }

    @Basic
    @Column(name = "checkID")
    public String getCheckId() {
        return checkId;
    }

    public void setCheckId(String checkId) {
        this.checkId = checkId;
    }

    @Basic
    @Column(name = "check_date")
    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "year")
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Basic
    @Column(name = "period")
    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Basic
    @Column(name = "stafflevel")
    public Integer getStafflevel() {
        return stafflevel;
    }

    public void setStafflevel(Integer stafflevel) {
        this.stafflevel = stafflevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LeaveMasterEntity that = (LeaveMasterEntity) o;

        if (leaveId != null ? !leaveId.equals(that.leaveId) : that.leaveId != null) return false;
        if (staffId != null ? !staffId.equals(that.staffId) : that.staffId != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (reason != null ? !reason.equals(that.reason) : that.reason != null) return false;
        if (days != null ? !days.equals(that.days) : that.days != null) return false;
        if (dateapply != null ? !dateapply.equals(that.dateapply) : that.dateapply != null) return false;
        if (datestart != null ? !datestart.equals(that.datestart) : that.datestart != null) return false;
        if (dateend != null ? !dateend.equals(that.dateend) : that.dateend != null) return false;
        if (supervisorId != null ? !supervisorId.equals(that.supervisorId) : that.supervisorId != null) return false;
        if (supervisorDate != null ? !supervisorDate.equals(that.supervisorDate) : that.supervisorDate != null)
            return false;
        if (headId != null ? !headId.equals(that.headId) : that.headId != null) return false;
        if (headDate != null ? !headDate.equals(that.headDate) : that.headDate != null) return false;
        if (hrId != null ? !hrId.equals(that.hrId) : that.hrId != null) return false;
        if (hrDate != null ? !hrDate.equals(that.hrDate) : that.hrDate != null) return false;
        if (checkId != null ? !checkId.equals(that.checkId) : that.checkId != null) return false;
        if (checkDate != null ? !checkDate.equals(that.checkDate) : that.checkDate != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (year != null ? !year.equals(that.year) : that.year != null) return false;
        if (period != null ? !period.equals(that.period) : that.period != null) return false;
        if (stafflevel != null ? !stafflevel.equals(that.stafflevel) : that.stafflevel != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = leaveId != null ? leaveId.hashCode() : 0;
        result = 31 * result + (staffId != null ? staffId.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        result = 31 * result + (days != null ? days.hashCode() : 0);
        result = 31 * result + (dateapply != null ? dateapply.hashCode() : 0);
        result = 31 * result + (datestart != null ? datestart.hashCode() : 0);
        result = 31 * result + (dateend != null ? dateend.hashCode() : 0);
        result = 31 * result + (supervisorId != null ? supervisorId.hashCode() : 0);
        result = 31 * result + (supervisorDate != null ? supervisorDate.hashCode() : 0);
        result = 31 * result + (headId != null ? headId.hashCode() : 0);
        result = 31 * result + (headDate != null ? headDate.hashCode() : 0);
        result = 31 * result + (hrId != null ? hrId.hashCode() : 0);
        result = 31 * result + (hrDate != null ? hrDate.hashCode() : 0);
        result = 31 * result + (checkId != null ? checkId.hashCode() : 0);
        result = 31 * result + (checkDate != null ? checkDate.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (period != null ? period.hashCode() : 0);
        result = 31 * result + (stafflevel != null ? stafflevel.hashCode() : 0);
        return result;
    }
}
