package com.padel.email.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "checkform", schema = "eleave", catalog = "")
public class CheckformEntity {
    private int id;
    private String checkId;
    private String question1;
    private String question2;
    private String question3;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "checkID")
    public String getCheckId() {
        return checkId;
    }

    public void setCheckId(String checkId) {
        this.checkId = checkId;
    }

    @Basic
    @Column(name = "question1")
    public String getQuestion1() {
        return question1;
    }

    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    @Basic
    @Column(name = "question2")
    public String getQuestion2() {
        return question2;
    }

    public void setQuestion2(String question2) {
        this.question2 = question2;
    }

    @Basic
    @Column(name = "question3")
    public String getQuestion3() {
        return question3;
    }

    public void setQuestion3(String question3) {
        this.question3 = question3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckformEntity that = (CheckformEntity) o;
        return id == that.id &&
                Objects.equals(checkId, that.checkId) &&
                Objects.equals(question1, that.question1) &&
                Objects.equals(question2, that.question2) &&
                Objects.equals(question3, that.question3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, checkId, question1, question2, question3);
    }
}
