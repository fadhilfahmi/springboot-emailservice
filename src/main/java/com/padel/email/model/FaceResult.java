package com.padel.email.model;

import lombok.Data;

@Data
public class FaceResult {
	
	private String table;
	private String action;
	private TempData data;
	

}
