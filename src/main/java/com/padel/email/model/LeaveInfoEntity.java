package com.padel.email.model;

import javax.persistence.*;

@Entity
@Table(name = "leave_info", schema = "eleave", catalog = "")
public class LeaveInfoEntity {
    private int id;
    private String staffId;
    private Integer eligibleleave;
    private String year;
    private Integer bf;
    private Integer mc;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @Basic
    @Column(name = "eligibleleave")
    public Integer getEligibleleave() {
        return eligibleleave;
    }

    public void setEligibleleave(Integer eligibleleave) {
        this.eligibleleave = eligibleleave;
    }

    @Basic
    @Column(name = "year")
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Basic
    @Column(name = "bf")
    public Integer getBf() {
        return bf;
    }

    public void setBf(Integer bf) {
        this.bf = bf;
    }

    @Basic
    @Column(name = "mc")
    public Integer getMc() {
        return mc;
    }

    public void setMc(Integer mc) {
        this.mc = mc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LeaveInfoEntity that = (LeaveInfoEntity) o;

        if (id != that.id) return false;
        if (staffId != null ? !staffId.equals(that.staffId) : that.staffId != null) return false;
        if (eligibleleave != null ? !eligibleleave.equals(that.eligibleleave) : that.eligibleleave != null)
            return false;
        if (year != null ? !year.equals(that.year) : that.year != null) return false;
        if (bf != null ? !bf.equals(that.bf) : that.bf != null) return false;
        if (mc != null ? !mc.equals(that.mc) : that.mc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (staffId != null ? staffId.hashCode() : 0);
        result = 31 * result + (eligibleleave != null ? eligibleleave.hashCode() : 0);
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (bf != null ? bf.hashCode() : 0);
        result = 31 * result + (mc != null ? mc.hashCode() : 0);
        return result;
    }
}
