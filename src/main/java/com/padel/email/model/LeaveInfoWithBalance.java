package com.padel.email.model;

import com.padel.email.model.CoStaffEntity;

import lombok.Data;

@Data
public class LeaveInfoWithBalance {
	
	private CoStaffEntity staffInfo;
	private double bal;
	private double balSickLeave;
	private double eligibleMonth;
	private LeaveInfoEntity leaveInfo;
	
}
