package com.padel.email.model;

import lombok.Data;

@Data
public class TempData {
	
	private int slot_card_records_id;
	private int physical_id;
	private int person_id;
	private int person_btype;
	private int card_read_id;
	private String card_number;
	private int event_type;
	private String record_storage_key;
	private int auth_result;
	private String swip_card_time;
	private String update_time;
	private String create_time;
	private int is_face_terminal_event;
	private String is_deleted;
	private int is_cleared;
	private int is_allow_cleared;
	private String swip_card_rev_time;
	private int time_offset;
	private int attendance_status;
	private int source_type;
	private String snap_pic_url;
	private String temp_data;
	private int temp_status;
	private int mask_status;

}
