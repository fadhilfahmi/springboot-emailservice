package com.padel.email.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "person", schema = "eleave", catalog = "")
public class PersonEntity {
    private int id;
    private int personGroupId;
    private int personType;
    private String personCode;
    private String givenName;
    private String familyName;
    private String fullName;
    private int gender;
    private String password;
    private String email;
    private String phoneNumber;
    private String photoUrl;
    private int photoIndex;
    private String smallPhotoUrl;
    private Timestamp startValidDate;
    private Timestamp endValidDate;
    private int type;
    private String remark;
    private Timestamp updateTime;
    private Timestamp createTime;
    private int isDeleted;
    private String objectGuid;
    private String temper;
    private int temperStatus;
    private int personFrom;
    private String rdn;
    private String dn;
    private long usnChanged;
    private int startTimeDiffer;
    private int endTimeDiffer;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "person_group_id")
    public int getPersonGroupId() {
        return personGroupId;
    }

    public void setPersonGroupId(int personGroupId) {
        this.personGroupId = personGroupId;
    }

    @Basic
    @Column(name = "person_type")
    public int getPersonType() {
        return personType;
    }

    public void setPersonType(int personType) {
        this.personType = personType;
    }

    @Basic
    @Column(name = "person_code")
    public String getPersonCode() {
        return personCode;
    }

    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    @Basic
    @Column(name = "given_name")
    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    @Basic
    @Column(name = "family_name")
    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    @Basic
    @Column(name = "full_name")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Basic
    @Column(name = "gender")
    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "photo_url")
    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Basic
    @Column(name = "photo_index")
    public int getPhotoIndex() {
        return photoIndex;
    }

    public void setPhotoIndex(int photoIndex) {
        this.photoIndex = photoIndex;
    }

    @Basic
    @Column(name = "small_photo_url")
    public String getSmallPhotoUrl() {
        return smallPhotoUrl;
    }

    public void setSmallPhotoUrl(String smallPhotoUrl) {
        this.smallPhotoUrl = smallPhotoUrl;
    }

    @Basic
    @Column(name = "start_valid_date")
    public Timestamp getStartValidDate() {
        return startValidDate;
    }

    public void setStartValidDate(Timestamp startValidDate) {
        this.startValidDate = startValidDate;
    }

    @Basic
    @Column(name = "end_valid_date")
    public Timestamp getEndValidDate() {
        return endValidDate;
    }

    public void setEndValidDate(Timestamp endValidDate) {
        this.endValidDate = endValidDate;
    }

    @Basic
    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "update_time")
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "is_deleted")
    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Basic
    @Column(name = "object_guid")
    public String getObjectGuid() {
        return objectGuid;
    }

    public void setObjectGuid(String objectGuid) {
        this.objectGuid = objectGuid;
    }

    @Basic
    @Column(name = "temper")
    public String getTemper() {
        return temper;
    }

    public void setTemper(String temper) {
        this.temper = temper;
    }

    @Basic
    @Column(name = "temper_status")
    public int getTemperStatus() {
        return temperStatus;
    }

    public void setTemperStatus(int temperStatus) {
        this.temperStatus = temperStatus;
    }

    @Basic
    @Column(name = "person_from")
    public int getPersonFrom() {
        return personFrom;
    }

    public void setPersonFrom(int personFrom) {
        this.personFrom = personFrom;
    }

    @Basic
    @Column(name = "rdn")
    public String getRdn() {
        return rdn;
    }

    public void setRdn(String rdn) {
        this.rdn = rdn;
    }

    @Basic
    @Column(name = "dn")
    public String getDn() {
        return dn;
    }

    public void setDn(String dn) {
        this.dn = dn;
    }

    @Basic
    @Column(name = "usn_changed")
    public long getUsnChanged() {
        return usnChanged;
    }

    public void setUsnChanged(long usnChanged) {
        this.usnChanged = usnChanged;
    }

    @Basic
    @Column(name = "start_time_differ")
    public int getStartTimeDiffer() {
        return startTimeDiffer;
    }

    public void setStartTimeDiffer(int startTimeDiffer) {
        this.startTimeDiffer = startTimeDiffer;
    }

    @Basic
    @Column(name = "end_time_differ")
    public int getEndTimeDiffer() {
        return endTimeDiffer;
    }

    public void setEndTimeDiffer(int endTimeDiffer) {
        this.endTimeDiffer = endTimeDiffer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonEntity that = (PersonEntity) o;
        return id == that.id &&
                personGroupId == that.personGroupId &&
                personType == that.personType &&
                gender == that.gender &&
                photoIndex == that.photoIndex &&
                type == that.type &&
                isDeleted == that.isDeleted &&
                temperStatus == that.temperStatus &&
                personFrom == that.personFrom &&
                usnChanged == that.usnChanged &&
                startTimeDiffer == that.startTimeDiffer &&
                endTimeDiffer == that.endTimeDiffer &&
                Objects.equals(personCode, that.personCode) &&
                Objects.equals(givenName, that.givenName) &&
                Objects.equals(familyName, that.familyName) &&
                Objects.equals(fullName, that.fullName) &&
                Objects.equals(password, that.password) &&
                Objects.equals(email, that.email) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(photoUrl, that.photoUrl) &&
                Objects.equals(smallPhotoUrl, that.smallPhotoUrl) &&
                Objects.equals(startValidDate, that.startValidDate) &&
                Objects.equals(endValidDate, that.endValidDate) &&
                Objects.equals(remark, that.remark) &&
                Objects.equals(updateTime, that.updateTime) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(objectGuid, that.objectGuid) &&
                Objects.equals(temper, that.temper) &&
                Objects.equals(rdn, that.rdn) &&
                Objects.equals(dn, that.dn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, personGroupId, personType, personCode, givenName, familyName, fullName, gender, password, email, phoneNumber, photoUrl, photoIndex, smallPhotoUrl, startValidDate, endValidDate, type, remark, updateTime, createTime, isDeleted, objectGuid, temper, temperStatus, personFrom, rdn, dn, usnChanged, startTimeDiffer, endTimeDiffer);
    }
}
