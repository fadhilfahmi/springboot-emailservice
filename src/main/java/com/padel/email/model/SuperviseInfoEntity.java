package com.padel.email.model;

import javax.persistence.*;

@Entity
@Table(name = "supervise_info", schema = "eleave", catalog = "")
public class SuperviseInfoEntity {
    private int id;
    private String staffId;
    private String supervisorId;
    private String headId;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @Basic
    @Column(name = "supervisorID")
    public String getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }

    @Basic
    @Column(name = "headID")
    public String getHeadId() {
        return headId;
    }

    public void setHeadId(String headId) {
        this.headId = headId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SuperviseInfoEntity that = (SuperviseInfoEntity) o;

        if (id != that.id) return false;
        if (staffId != null ? !staffId.equals(that.staffId) : that.staffId != null) return false;
        if (supervisorId != null ? !supervisorId.equals(that.supervisorId) : that.supervisorId != null) return false;
        if (headId != null ? !headId.equals(that.headId) : that.headId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (staffId != null ? staffId.hashCode() : 0);
        result = 31 * result + (supervisorId != null ? supervisorId.hashCode() : 0);
        result = 31 * result + (headId != null ? headId.hashCode() : 0);
        return result;
    }
}
