package com.padel.email.model;

import javax.persistence.*;

@Entity
@Table(name = "user_access", schema = "eleave", catalog = "")
public class UserAccessEntity {
    private int id;
    private String staffId;
    private int level;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @Basic
    @Column(name = "level")
    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAccessEntity that = (UserAccessEntity) o;

        if (id != that.id) return false;
        if (level != that.level) return false;
        if (staffId != null ? !staffId.equals(that.staffId) : that.staffId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (staffId != null ? staffId.hashCode() : 0);
        result = 31 * result + level;
        return result;
    }
}
