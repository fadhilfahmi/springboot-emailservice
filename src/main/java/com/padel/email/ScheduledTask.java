package com.padel.email;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.padel.email.model.CheckmasterEntity;
import com.padel.email.model.CoStaffEntity;
import com.padel.email.model.IOData;
import com.padel.email.model.IOSummary;
import com.padel.email.service.CheckService;

@Component
public class ScheduledTask {

	@Autowired
	private SendemailserviceApplication sendService;

	@Autowired
	private CheckService checkService;

	private static final Logger log = LoggerFactory.getLogger(ScheduledTask.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	private static final SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

	//@Scheduled(fixedRate = 10000)
	// @Scheduled(cron = "0 0 9 * * MON")
	// @Scheduled(cron = "0 0 22 * * SAT")
	@Scheduled(cron = "0 0 10 * * MON")
	public void sendScheduleEmail() throws AddressException, MessagingException, IOException, ParseException {

		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.setTime(date);
		int i = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
		c.add(Calendar.DATE, -i - 7);
		Date start = c.getTime();
		c.add(Calendar.DATE, 6);
		Date end = c.getTime();
		System.out.println(String.valueOf(dateFormat1.format(start)) + " - " + dateFormat1.format(end));

		List<CoStaffEntity> sv = checkService.getAllHQStaff();

		int in = 0;

		for (CoStaffEntity d : sv) {

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date startDate = formatter.parse(dateFormat1.format(start));
			Date endDate = formatter.parse(dateFormat1.format(end));

			Calendar start2 = Calendar.getInstance();
			start2.setTime(startDate);
			Calendar end2 = Calendar.getInstance();
			end2.setTime(endDate);

			IOSummary ios = new IOSummary();
			List<IOData> CV;
			CV = new ArrayList();

			for (Date datex = start2.getTime(); start2.before(end2); start2.add(Calendar.DATE,
					1), datex = start2.getTime()) {

				IOData iod = new IOData();
				String workingDate = String.valueOf(dateFormat1.format(datex));

				CheckmasterEntity cm = checkService.getCheckInfo(d.getStaffid(), workingDate);
				if (cm != null) {
					iod.setTimein(cm.getTimevalid());
					iod.setTimout(cm.getTimeout());
					iod.setDate(cm.getDate());

					IOData iod1 = workingHour(cm.getTimevalid(), cm.getTimeout(), cm.getDate());
					iod.setDuration(iod1.getDuration());
					iod.setColorduration(iod1.getColorduration());
					iod.setColortimein(iod1.getColortimein());
					iod.setColortimout(iod1.getColortimout());

					CV.add(iod);

					log.info("rekod date :  {}", cm.getDate());
					log.info("Nama :  {}", d.getName());
				}

			}

			ios.setIo(CV);

			String body = emailBody(ios, d);

			EmailMessage em = new EmailMessage();
			em.setSenderEmail("sysadmin@lcsb.com.my");
			em.setSenderPassword("adminlcsb");
			em.setSubject("Laporan Mingguan Rekod Kehadiran");
			em.setTo_address(d.getEmail());
			em.setBody(body);
			sendService.sendmail(em);
			log.info("Email sent @ {}", dateFormat.format(new Date()));

		}

		log.info("All Staff :  {}", sv.size());
		log.info("In Staff :  {}", in);

	}

	// 0 0/30 * * * ?
	// @Scheduled(cron = "0 0 10 * * MON-FRI")
	@Scheduled(fixedRate = 50000)
	public void reportCurrentTimeq() {
		// log.info("The hahaha {}", dateFormat.format(new Date()));

	}

	// @Scheduled(fixedRate = 10000)
	@Scheduled(cron = "0 0 10 * * MON,FRI,SAT")
	public void dailyScheduleJob() {
		/*
		 * your code here
		 */
	}

	private static IOData workingHour(String timein, String timeout, String date) throws ParseException {

		IOData io = new IOData();

		Date timeIn = dateFormat.parse(timein);
		Date timeOut = dateFormat.parse(timeout);

		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat formatD = new SimpleDateFormat("yyyy-MM-dd");

		Date timeInMax = null;
		Date timeInMaxRange = null;

		Date timeInMax1 = null;
		Date timeInMaxRange1 = null;
		Date timeInMax2 = null;

		Date timeOutMin1 = null;
		Date timeOutMin2 = null;

		Date lastFasting = formatD.parse("2020-05-24");
		Date thisDate = formatD.parse(date);
		Date normalWorkHour = formatD.parse("2020-06-10");

		if (thisDate.compareTo(lastFasting) > 0) {

			timeInMax = format.parse("08:31:00");
			timeInMaxRange = format.parse("10:00:00");

			timeInMax1 = format.parse("11:30:00");
			timeInMaxRange1 = format.parse("14:00:00");
			timeInMax2 = format.parse("13:01:00");

			timeOutMin1 = format.parse("12:30:00");
			timeOutMin2 = format.parse("17:00:00");

		}

		if (thisDate.compareTo(lastFasting) < 0) {

			timeInMax = format.parse("08:16:00");
			timeInMaxRange = format.parse("10:00:00");

			timeInMax1 = format.parse("11:30:00");
			timeInMaxRange1 = format.parse("14:00:00");
			timeInMax2 = format.parse("12:31:00");

			timeOutMin1 = format.parse("12:15:00");
			timeOutMin2 = format.parse("16:30:00");

		}

		if (thisDate.compareTo(normalWorkHour) > 0) {

			timeInMax = format.parse("08:16:00");

			timeOutMin1 = format.parse("17:00:00");

		}

		if (thisDate.compareTo(normalWorkHour) == 0) {

			timeInMax = format.parse("08:16:00");

			timeOutMin1 = format.parse("17:00:00");

		}

		Date timeOutZero = format.parse("00:00:00");

		if (thisDate.compareTo(normalWorkHour) < 0) {

			if ((timeIn.before(timeInMaxRange) && timeIn.after(timeInMax)) || timeIn.after(timeInMax2)) {
				// masuk lewat
				log.info("Late In :  {}", timeIn);
				io.setColortimein("red");

			} else {
				io.setColortimein("green");
			}

			if (timeOut.before(timeOutMin1)
					|| ((timeIn.after(timeInMax1) && timeIn.before(timeInMaxRange1)) && timeOut.before(timeOutMin2))) {
				// masuk lewat
				log.info("Early Out :  {}", timeOut);
				io.setColortimout("red");

			} else {
				io.setColortimout("green");
			}
			
			long difference = timeOut.getTime() - timeIn.getTime();

    		long diffInMin = difference / (60 * 1000);
    		long diffMinutes = difference / (60 * 1000) % 60;
    		long diffHours = difference / (60 * 60 * 1000) % 24;

    		io.setDuration(String.valueOf(diffHours + "j " + diffMinutes + "m"));

    		if (diffInMin > 241) {
    			io.setCompleteHour(true);
    			io.setColorduration("green");
    		} else {
    			io.setCompleteHour(false);
    			io.setColorduration("red");
    		}

    		if (timeout.equals("00:00:00")) {
    			io.setDuration("N/A");
    		}
		}else {
			


            //if (timeIn.after(timeInMax) || timeOut.before(timeOutMin1)) {
                //if (((timeIn.before(timeInMaxRange) && timeIn.after(timeInMax)) || timeIn.after(timeInMax2)) || (timeOut.before(timeOutMin1) || ((timeIn.after(timeInMax1) && timeIn.before(timeInMaxRange1)) && timeOut.before(timeOutMin2)))) {

                if (timeIn.after(timeInMax)) {
                    //masuk lewat

                    io.setColortimein("red");

                } else {
                    io.setColortimein("green");
                }

                if (timeOut.before(timeOutMin1)) {

                    io.setColortimout("red");

                } else {
                    io.setColortimout("green");
                }

                long difference = timeOut.getTime() - timeIn.getTime();

        		long diffInMin = difference / (60 * 1000);
        		long diffMinutes = difference / (60 * 1000) % 60;
        		long diffHours = difference / (60 * 60 * 1000) % 24;

        		io.setDuration(String.valueOf(diffHours + "j " + diffMinutes + "m"));

        		if (diffInMin > 526) {
        			io.setCompleteHour(true);
        			io.setColorduration("green");
        		} else {
        			io.setCompleteHour(false);
        			io.setColorduration("red");
        		}

        		if (timeout.equals("00:00:00")) {
        			io.setDuration("N/A");
        		}

            //}
		}

		

		/*
		 * if (((timeIn.compareTo(timeInMax) > 0 && timeIn.compareTo(timeInMaxRange) <
		 * 0) || timeIn.compareTo(timeInMax2) > 0) || ((timeOut.compareTo(timeOutMin1) <
		 * 0 || ((timeIn.compareTo(timeInMax1) > 0 && timeIn.compareTo(timeInMaxRange1)
		 * < 0) && timeOut.compareTo(timeOutMin2) < 0)) &&
		 * timeOut.compareTo(timeOutZero) != 0)) {
		 * 
		 * if (((timeIn.compareTo(timeInMax) > 0 && timeIn.compareTo(timeInMaxRange) <
		 * 0) || timeIn.compareTo(timeInMax2) > 0)) { io.setColortimein("red"); } else {
		 * io.setColortimein("green"); } // 12:22 compare 12:30:00 || 08:33:00 compare
		 * 11:30:00 08:33:00 compare 14:00:00 if ((timeOut.compareTo(timeOutMin1) < 0 ||
		 * ((timeIn.compareTo(timeInMax1) > 0 && timeIn.compareTo(timeInMaxRange1) < 0)
		 * && timeOut.compareTo(timeOutMin2) < 0)) && timeOut.compareTo(timeOutZero) !=
		 * 0) { io.setColortimout("red"); } else { io.setColortimout("green"); }
		 * 
		 * long difference = timeOut.getTime() - timeIn.getTime();
		 * 
		 * long diffInMin = difference / (60 * 1000); long diffMinutes = difference /
		 * (60 * 1000) % 60; long diffHours = difference / (60 * 60 * 1000) % 24;
		 * 
		 * io.setDuration(String.valueOf(diffHours + "j " + diffMinutes + "m"));
		 * 
		 * if (diffInMin > 241) { io.setCompleteHour(true);
		 * io.setColorduration("green"); } else { io.setCompleteHour(false);
		 * io.setColorduration("red"); }
		 * 
		 * }
		 */

		return io;
	}

	private static String emailBody(IOSummary cm, CoStaffEntity st) {

		String body = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
				+ "<html xmlns:v=\"urn:schemas-microsoft-com:vml\">\n" + "\n" + "<head>\n"
				+ "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
				+ "    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n"
				+ "    <!--[if !mso]--><!-- -->\n"
				+ "    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel=\"stylesheet\">\n"
				+ "    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel=\"stylesheet\">\n"
				+ "    <!--<![endif]-->\n" + "\n" + "    <title>Material Design for Bootstrap</title>\n" + "\n"
				+ "    <style type=\"text/css\">\n" + "        body {\n" + "            width: 100%;\n"
				+ "            background-color: #ffffff;\n" + "            margin: 0;\n" + "            padding: 0;\n"
				+ "            -webkit-font-smoothing: antialiased;\n" + "            mso-margin-top-alt: 0px;\n"
				+ "            mso-margin-bottom-alt: 0px;\n" + "            mso-padding-alt: 0px 0px 0px 0px;\n"
				+ "        }\n" + "\n" + "        p,\n" + "        h1,\n" + "        h2,\n" + "        h3,\n"
				+ "        h4 {\n" + "            margin-top: 0;\n" + "            margin-bottom: 0;\n"
				+ "            padding-top: 0;\n" + "            padding-bottom: 0;\n" + "        }\n" + "\n"
				+ "        span.preheader {\n" + "            display: none;\n" + "            font-size: 1px;\n"
				+ "        }\n" + "\n" + "        html {\n" + "            width: 100%;\n" + "        }\n" + "\n"
				+ "        table {\n" + "            font-size: 14px;\n" + "            border: 0;\n" + "        }\n"
				+ "\n" + ".img-circular{\n" + " width: 60px;\n" + " height: 60px;\n" + " background-image: url('"
				+ st.getImageUrl() + "');\n" + " background-size: cover;\n" + " display: block;\n"
				+ " border-radius: 100px;\n" + " -webkit-border-radius: 100px;\n" + " -moz-border-radius: 100px;\n"
				+ "}\n" + "\n" + "        /* ----------- responsivity ----------- */\n" + "\n"
				+ "        @media only screen and (max-width: 640px) {\n"
				+ "            /*------ top header ------ */\n" + "            .main-header {\n"
				+ "                font-size: 20px !important;\n" + "            }\n"
				+ "            .main-section-header {\n" + "                font-size: 28px !important;\n"
				+ "            }\n" + "            .show {\n" + "                display: block !important;\n"
				+ "            }\n" + "            .hide {\n" + "                display: none !important;\n"
				+ "            }\n" + "            .align-center {\n"
				+ "                text-align: center !important;\n" + "            }\n" + "            .no-bg {\n"
				+ "                background: none !important;\n" + "            }\n"
				+ "            /*----- main image -------*/\n" + "            .main-image img {\n"
				+ "                width: 440px !important;\n" + "                height: auto !important;\n"
				+ "            }\n" + "            /* ====== divider ====== */\n" + "            .divider img {\n"
				+ "                width: 440px !important;\n" + "            }\n"
				+ "            /*-------- container --------*/\n" + "            .container590 {\n"
				+ "                width: 440px !important;\n" + "            }\n" + "            .container580 {\n"
				+ "                width: 400px !important;\n" + "            }\n" + "            .main-button {\n"
				+ "                width: 220px !important;\n" + "            }\n"
				+ "            /*-------- secions ----------*/\n" + "            .section-img img {\n"
				+ "                width: 320px !important;\n" + "                height: auto !important;\n"
				+ "            }\n" + "            .team-img img {\n" + "                width: 100% !important;\n"
				+ "                height: auto !important;\n" + "            }\n" + "        }\n" + "\n"
				+ "        @media only screen and (max-width: 479px) {\n"
				+ "            /*------ top header ------ */\n" + "            .main-header {\n"
				+ "                font-size: 18px !important;\n" + "            }\n"
				+ "            .main-section-header {\n" + "                font-size: 26px !important;\n"
				+ "            }\n" + "            /* ====== divider ====== */\n" + "            .divider img {\n"
				+ "                width: 280px !important;\n" + "            }\n"
				+ "            /*-------- container --------*/\n" + "            .container590 {\n"
				+ "                width: 280px !important;\n" + "            }\n" + "            .container590 {\n"
				+ "                width: 280px !important;\n" + "            }\n" + "            .container580 {\n"
				+ "                width: 260px !important;\n" + "            }\n"
				+ "            /*-------- secions ----------*/\n" + "            .section-img img {\n"
				+ "                width: 280px !important;\n" + "                height: auto !important;\n"
				+ "            }\n" + "        }\n" + "    </style>\n"
				+ "    <!--[if gte mso 9]><style type=”text/css”>\n" + "        body {\n"
				+ "        font-family: arial, sans-serif!important;\n" + "        }\n" + "        </style>\n"
				+ "    <![endif]-->\n" + "</head>\n" + "\n" + "\n"
				+ "<body class=\"respond\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n"
				+ "    <!-- pre-header -->\n" + "    <!--<table style=\"display:none!important;\">\n" + "        <tr>\n"
				+ "            <td>\n"
				+ "                <div style=\"overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;\">\n"
				+ "                    Welcome to MDB!\n" + "                </div>\n" + "            </td>\n"
				+ "        </tr>\n" + "    </table>-->\n" + "    <!-- pre-header end -->\n" + "    <!-- header -->\n"
				+ "    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\">\n"
				+ "\n" + "        <tr>\n" + "            <td align=\"center\">\n"
				+ "                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
				+ "\n" + "                    <tr>\n"
				+ "                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n"
				+ "                    </tr>\n" + "\n" + "                    <tr>\n"
				+ "                        <td align=\"center\">\n" + "\n"
				+ "                            <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
				+ "\n" + "                                <tr>\n"
				+ "                        <td align=\"center\" style=\"color: #343434; font-size: 40px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n"
				+ "                            class=\"main-header\">\n"
				+ "                            <!-- section text ======-->\n" + "\n"
				+ "                            <div style=\"line-height: 35px\">\n" + "\n"
				+ "                                e<span style=\"color: #5caad2;\">Daftar</span>\n" + "\n"
				+ "                            </div>\n" + "                                    </td>\n"
				+ "                                </tr>\n" + "\n" + "                                <tr>\n"
				+ "                                    <td align=\"center\">\n"
				+ "                                        <table width=\"360 \" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n"
				+ "                                            class=\"container590 hide\">\n"
				+ "                                            <tr>\n"
				+ "                                                <td width=\"120\" align=\"center\" style=\"font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
				+ "                                                    Sistem Rekod Kehadiran & Saringan Kesihatan \n"
				+ "                                                </td>\n"
				+ "                                            </tr>\n"
				+ "                                        </table>\n" + "                                    </td>\n"
				+ "                                </tr>\n" + "                            </table>\n"
				+ "                        </td>\n" + "                    </tr>\n" + "\n"
				+ "                    <tr>\n"
				+ "                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n"
				+ "                    </tr>\n" + "\n" + "                </table>\n" + "            </td>\n"
				+ "        </tr>\n" + "    </table>\n" + "    <!-- end header -->\n" + "\n"
				+ "    <!-- big image section -->\n" + "\n"
				+ "    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\" class=\"bg_color\">\n"
				+ "\n" + "        <tr>\n" + "            <td align=\"center\">\n"
				+ "                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
				+ "\n" + "                    <tr>\n"
				+ "                        <td align=\"center\" style=\"color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n"
				+ "                            class=\"main-header\">\n"
				+ "                            <!-- section text ======-->\n" + "                            \n"
				+ "                            <div class=\"img-circular\"></div>\n"
				+ "                            <div style=\"line-height: 35px\">\n" + "\n"
				+ "                                Assalamualaikum <span style=\"color: #5caad2;\">" + st.getName()
				+ "</span>\n" + "\n" + "                            </div>\n" + "                        </td>\n"
				+ "                    </tr>\n" + "\n" + "                    <tr>\n"
				+ "                        <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
				+ "                    </tr>\n" + "\n" + "                    <tr>\n"
				+ "                        <td align=\"center\">\n"
				+ "                            <table border=\"0\" width=\"40\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"eeeeee\">\n"
				+ "                                <tr>\n"
				+ "                                    <td height=\"2\" style=\"font-size: 2px; line-height: 2px;\">&nbsp;</td>\n"
				+ "                                </tr>\n" + "                            </table>\n"
				+ "                        </td>\n" + "                    </tr>\n" + "\n"
				+ "                    <tr>\n"
				+ "                        <td height=\"20\" style=\"font-size: 20px; line-height: 20px;\">&nbsp;</td>\n"
				+ "                    </tr>\n" + "\n" + "                    <tr>\n"
				+ "                        <td align=\"left\">\n"
				+ "                            <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
				+ "                                <tr>\n"
				+ "                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
				+ "                                        <!-- section text ======-->\n" + "\n"
				+ "                                        <p style=\"line-height: 24px; margin-bottom:15px;\">\n"
				+ "\n" + "                                            Tuan/Puan,\n" + "\n"
				+ "                                        </p>\n"
				+ "                                        <p style=\"line-height: 24px;margin-bottom:15px;\">\n"
				+ "                                            Berikut adalah rekod kedatangan anda pada minggu lepas.\n"
				+ "                                        </p>\n" + "                                       \n"
				+ "                                        \n" + "                                        \n"
				+ "                                    </td>\n" + "                                </tr>\n"
				+ "                                <tr>\n"
				+ "                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
				+ "                                        <!-- section text ======-->\n" + "\n"
				+ "                                       <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n"
				+ "                                        <tr>\n"
				+ "                                            <th>Tarikh</th>\n"
				+ "                                            <th>Masuk</th>\n"
				+ "                                            <th>Keluar</th>\n"
				+ "                                            <th>Tempoh</th>\n"
				+ "                                        </tr>\n";
		List<IOData> io = cm.getIo();
		for (IOData d : io) {
			body += "                                			<tr>\n"
					+ "                                				<td align=\"center\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>"
					+ d.getDate() + "</strong></p></td>\n"
					+ "                                				<td align=\"center\" style=\"color: "
					+ d.getColortimein()
					+ "; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
					+ d.getTimein() + "</strong></p></td>\n"
					+ "                                				<td align=\"center\" style=\"color: "
					+ d.getColortimout()
					+ "; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
					+ d.getTimout() + "</strong></p></td>\n"
					+ "                                				<td align=\"center\" style=\"color: "
					+ d.getColorduration()
					+ "; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
					+ d.getDuration() + "</strong></p></td>\n" + "\n"
					+ "                                			</tr>\n";
		}
		body += "                                		</table>\n"
				+ "                                		<p>&nbsp;</p>\n"
				+ "                                		 <p style=\"line-height: 24px; margin-bottom:20px;\">\n"
				+ "                                            Terima kasih atas kerjasama anda.\n"
				+ "                                        </p>\n" + "                                        \n"
				+ "                                        \n" + "                                    </td>\n"
				+ "                                </tr>\n" + "                                <tr>\n"
				+ "                                	<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n"
				+ "                                		<br>\n"
//				+ "                                		<table border=\"0\" align=\"center\" width=\"180\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"5caad2\" style=\"margin-bottom:20px;\">\n"
//				+ "\n" + "                                            <tr>\n"
//				+ "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
//				+ "                                            </tr>\n" + "\n"
//				+ "                                            <tr>\n"
//				+ "                                                <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\">\n"
//				+ "                                                    <!-- main section button -->\n" + "\n"
//				+ "                                                    <div style=\"line-height: 22px;\">\n"
//				+ "                                                        <a href=\"https://edaftar.lcsb.com.my/Login?linkTo=viewrekodkedatangan&checkID=\" style=\"color: #ffffff; text-decoration: none;\">Pergi ke eDaftar</a>\n"
//				+ "                                                    </div>\n"
//				+ "                                                </td>\n"
//				+ "                                            </tr>\n" + "\n"
//				+ "                                            <tr>\n"
//				+ "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n"
//				+ "                                            </tr>\n" + "\n"
//				+ "                                        </table>\n"
//				+ "                                		<p>&nbsp;</p>\n"
				+ "                                		<p style=\"line-height: 24px\">\n"
				+ "                                            Yang Benar,\n"
				+ "                                        </p>\n"
				+ "										<p style=\"line-height: 24px\">\n"
				+ "                                            <strong>LCSB Bot</strong>\n"
				+ "                                        </p>\n"
				+ "                                        <p style=\"line-height: 24px\">\n"
				+ "                                            Pentadbir Maya Sistem eDaftar\n"
				+ "                                        </p>\n" + "\n" + "                                	</td>\n"
				+ "                                </tr>\n" + "                            </table>\n"
				+ "                        </td>\n" + "                    </tr>\n" + "\n" + "\n" + "\n" + "\n" + "\n"
				+ "                </table>\n" + "\n" + "            </td>\n" + "        </tr>\n" + "\n"
				+ "        <tr>\n"
				+ "            <td height=\"40\" style=\"font-size: 40px; line-height: 40px;\">&nbsp;</td>\n"
				+ "        </tr>\n" + "\n" + "    </table>\n" + "\n" + "    <!-- end section -->\n" + "\n" + "\n"
				+ "    <!-- main section -->\n" + "    \n" + "\n" + "    <!-- end section -->\n" + "\n" + "    \n"
				+ "    <!-- end section -->\n" + "\n" + "    <!-- footer ====== -->\n"
				+ "    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table>\n"
				+ "    <!-- end footer ====== -->\n" + "\n" + "</body>\n" + "\n" + "</html>";
		return body;

	}

	

}
