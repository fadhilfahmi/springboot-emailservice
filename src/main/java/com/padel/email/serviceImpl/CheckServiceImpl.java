package com.padel.email.serviceImpl;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.impossibl.postgres.api.jdbc.PGConnection;
import com.impossibl.postgres.api.jdbc.PGNotificationListener;
import com.impossibl.postgres.jdbc.PGDataSource;
import com.padel.email.ScheduledTask;
import com.padel.email.UpdateeDaftar;
import com.padel.email.model.CheckmasterEntity;
import com.padel.email.model.CoStaffEntity;
import com.padel.email.model.FaceResult;
import com.padel.email.repository.CheckRepository;
import com.padel.email.service.CheckService;



@Service
@Configurable
public class CheckServiceImpl implements CheckService {
	
	
	
	private static final Logger log = LoggerFactory.getLogger(CheckServiceImpl.class);

	@Autowired
	private CheckService checkService;
	
	@Autowired
    private CheckRepository repository;
	
	@Override
    public List<CoStaffEntity> getAllHQStaff() {
		
		return repository.getAllHQStaff();
    }
	
	@Override
    public CheckmasterEntity getCheckInfo(String staffId, String date){
		
		return repository.getCheckInfo(staffId, date);
    }
	
	@Override
	public String getStaffID(int id) {
		
		return repository.getStaffID(id);
	}

	@Override
	public void update(double temp, String status, String time, String date, String staffId) {
		// TODO Auto-generated method stub
		repository.update(temp, status, time,date, staffId);
	}

	@Override
	public CheckmasterEntity create(CheckmasterEntity cm) {
		
		return repository.save(cm);
	}

	@Override
	public String getMaxID() {
		// TODO Auto-generated method stub
		return repository.getMaxID();
	}

	
	

	/*
	 * @Override public String saveForVIP(String id, String staffId, String date,
	 * String timescan, String timevalid, double temp, String status) {
	 * 
	 * id = repository.getMaxID();
	 * 
	 * 
	 * 
	 * repository.saveForVIP(id, staffId, date, timescan, timevalid, temp, status);
	 * 
	 * return id;
	 * 
	 * }
	 */

	
	
	

}
