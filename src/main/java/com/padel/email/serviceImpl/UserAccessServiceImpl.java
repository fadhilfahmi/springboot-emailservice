package com.padel.email.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padel.email.model.UserAccessEntity;
import com.padel.email.repository.UserAccessRepository;
import com.padel.email.service.UserAccessService;



@Service
public class UserAccessServiceImpl implements UserAccessService {
	
	@Autowired
    private UserAccessRepository repository;
	
	@Override
    public UserAccessEntity findByStaffId(String code) {
        return repository.findByStaffId(code);
    }

}
