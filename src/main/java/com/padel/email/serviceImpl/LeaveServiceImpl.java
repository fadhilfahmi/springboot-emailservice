package com.padel.email.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padel.email.model.LeaveMasterEntity;
import com.padel.email.model.CoStaffEntity;
import com.padel.email.model.SuperviseInfoEntity;
import com.padel.email.repository.LeaveRepository;
import com.padel.email.repository.StaffRepository;
import com.padel.email.service.LeaveService;
import com.padel.email.service.StaffService;
import com.padel.email.service.SuperviseService;
import com.padel.email.service.UserAccessService;

@Service
public class LeaveServiceImpl implements LeaveService {
	
	@Autowired
    private LeaveRepository repository;
	
	@Autowired
	private SuperviseService superviseService;
	
	@Autowired
	private UserAccessService uaService;
	
	@Override
    public List<LeaveMasterEntity> findAll() {
        return repository.findAll();
    }
	
	@Override
    public LeaveMasterEntity findByLeaveId(String code) {
        return repository.findByLeaveId(code);
    }
	
	@Override
    public List<LeaveMasterEntity> findByStaffId(String code) {
        return repository.findByStaffId(code);
    }
	
	@Override
    public List<LeaveMasterEntity> findLeaveNotApproved(String staffId) {
        return repository.findLeaveNotApproved(staffId);
    }
	
	@Override
    public List<LeaveMasterEntity> getNewLeave(String staffId) {
        return repository.getNewLeave(staffId);
    }
	
	@Override
    public List<LeaveMasterEntity> getLeaveWithStatus(String staffId, String status) {
        return repository.getLeaveWithStatus(staffId, status);
    }

	@Override
	public double getTotalLeaveOfTheYear(String staffId, String year) {
		
		return repository.getTotalLeaveOfTheYear(staffId, year);
	}
	
	@Override
	public double getTotalSickLeaveUseOfTheYear(String staffId, String year) {
		
		return repository.getTotalSickLeaveUseOfTheYear(staffId, year);
	}

	@Override
    public List<LeaveMasterEntity> getAllLeaveExcludeYou(String staffId) {
		
		
    	
    	List<LeaveMasterEntity> lm = new ArrayList<LeaveMasterEntity>();
    	
    	List<String> sv = superviseService.getSuperviseeIDs(staffId, uaService.findByStaffId(staffId).getLevel());
    	
    	for(int j = 0; j < sv.size(); j++){  
    		
    		System.out.print(sv.get(j));
    	
    		lm.addAll(repository.getAllLeaveExcludeYou(staffId, sv.get(j)));
    	}
		
		return lm;
    }

	@Override
	public List<LeaveMasterEntity> getUnapproveLeave() {
		return repository.getUnapprovedLeave();
	}
	
	

}
