package com.padel.email.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padel.email.model.CoStaffEntity;
import com.padel.email.model.StaffModel;
import com.padel.email.repository.StaffRepository;
import com.padel.email.service.StaffService;
import com.padel.email.service.UserAccessService;




@Service
public class StaffServiceImpl implements StaffService {
	
	@Autowired
    private StaffRepository repository;
	
	@Autowired
    private UserAccessService serviceUserAccess;
	
	@Override
    public List<CoStaffEntity> findAll() {
        return repository.findAll();
    }
	
	@Override
    public CoStaffEntity findByStaffid(String code) {
        return repository.findByStaffid(code);
    }
	
	@Override
    public CoStaffEntity findByEmail(String code) {
        return repository.findByEmail(code);
    }
	
	
	public StaffModel getStaffInfoWithUserAccess(String code) {
		
		StaffModel sm = new StaffModel();
		sm.setCoStaff(repository.findByStaffid(code)); 
		sm.setUserLevel(serviceUserAccess.findByStaffId(code).getLevel());
		
        return sm;
    }
	
}
