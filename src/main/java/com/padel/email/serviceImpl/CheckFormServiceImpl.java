package com.padel.email.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padel.email.model.CheckformEntity;
import com.padel.email.repository.CheckFormRepository;
import com.padel.email.service.CheckInfoService;

@Service
public class CheckFormServiceImpl implements CheckInfoService {

	@Autowired
    private CheckFormRepository repository;
	
	@Override
	public CheckformEntity getCheckFormInfo(String checkId){
		
		return repository.getCheckFormInfo(checkId);
    }

	@Override
	public CheckformEntity create(CheckformEntity cf) {
		return repository.save(cf);
		
	}
}
