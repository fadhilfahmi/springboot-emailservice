package com.padel.email.serviceImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padel.email.model.SuperviseInfoEntity;
import com.padel.email.repository.SuperviseRepository;
import com.padel.email.service.SuperviseService;



@Service
public class SuperviseServiceImpl implements SuperviseService {
	
	@Autowired
    private SuperviseRepository repositorySupervise;

	@Override
	public List<String> getSuperviseeIDs(String code, int level) {
		
		List<String> listA = new ArrayList<String>();
		
		if(level == 2) {
			listA = repositorySupervise.getSuperviseeIDforSupervisor(code);
			
		}else if(level == 3) {
			listA = repositorySupervise.getSuperviseeIDforHead(code);
		}
		
		return listA;
	}

	@Override
	public SuperviseInfoEntity getSuperViseInfo(String code) {
		// TODO Auto-generated method stub
		return repositorySupervise.getSuperViseInfo(code);
	}

}
