package com.padel.email.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padel.email.model.LeaveInfoEntity;
import com.padel.email.repository.LeaveInfoRepository;
import com.padel.email.repository.LeaveRepository;
import com.padel.email.service.LeaveInfoService;
import com.padel.email.service.LeaveService;



@Service
public class LeaveInfoServiceImpl implements LeaveInfoService {
	
	@Autowired
    private LeaveInfoRepository repository;
	
	@Autowired
    private LeaveRepository repositoryLeaveMaster;
	
	@Autowired
    private LeaveService serviceLeaveMaster;
	
	@Override
    public LeaveInfoEntity findByStaffId(String code) {
        return repository.findByStaffId(code);
    }
	
	@Override
	public double getBalanceEligibleLeave(String code, String year) {
		
		double cnt = 0.0;
		LeaveInfoEntity l = repository.findByStaffIdAndYear(code, year);
		cnt = l.getEligibleleave() + l.getBf() - repositoryLeaveMaster.getTotalLeaveOfTheYear(code, year);
		
        return cnt;
    }

	@Override
	public LeaveInfoEntity findByStaffIdAndYear(String code, String year) {
		// TODO Auto-generated method stub
		return repository.findByStaffIdAndYear(code, year);
	}
	
	@Override
	public double getEligibleLeaveForTheMonth(String code, String year, String date) {
		
		double cnt = 0.0;
		LeaveInfoEntity l = repository.findByStaffIdAndYear(code, year);
		double CL = l.getEligibleleave();
		double curMonth = 3.0;
		double beforeAddCF = CL / 12 * curMonth; 
		double afterAddCF = beforeAddCF + l.getBf();
		double plusLeaveUse = afterAddCF - repositoryLeaveMaster.getTotalLeaveOfTheYear(code, year);
		
		cnt = plusLeaveUse;
		
        return cnt;
    }
	
	
	/*
	 * double i = 0;
	 * 
	 * double CL = LeaveDAO.getEligibleLeaveWithoutCF(log, staffID, year); double
	 * curMonth = Double.parseDouble(AccountingPeriod.getCurPeriodByDate(date));
	 * 
	 * double beforeAddCF = CL / 12 * curMonth; double afterAddCF = beforeAddCF +
	 * LeaveDAO.getPreviousLeaveCF(log, staffID, year); double plusLeaveUse =
	 * afterAddCF - LeaveDAO.getTotalLeaveOfTheYear(log, staffID, year);
	 */
	
	

}
