package com.padel.email.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.padel.email.model.SuperviseInfoEntity;


public interface SuperviseRepository   extends Repository<SuperviseInfoEntity, Integer> {
	
	@Query("SELECT p.staffId FROM SuperviseInfoEntity p WHERE p.supervisorId = ?1")
    List<String> getSuperviseeIDforSupervisor(String staffId);
	
	@Query("SELECT p.staffId FROM SuperviseInfoEntity p WHERE p.headId = ?1")
	List<String> getSuperviseeIDforHead(String staffId);
	
	@Query("SELECT p FROM SuperviseInfoEntity p WHERE p.staffId = ?1")
	SuperviseInfoEntity getSuperViseInfo(String staffId);

}
