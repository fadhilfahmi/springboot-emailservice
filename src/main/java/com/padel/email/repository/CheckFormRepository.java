package com.padel.email.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.padel.email.model.CheckformEntity;
import com.padel.email.model.CheckmasterEntity;

public interface CheckFormRepository  extends Repository<CheckformEntity, Integer> {
	
	@Query("SELECT p FROM  CheckformEntity p WHERE p.checkId = ?1")
	CheckformEntity getCheckFormInfo(String checkId);
	
//	@Transactional
//	@Modifying
//	@Query("INSERT INTO CheckformEntity u (u.checkId, u.question1, u.question2, u.question3) VALUES (?1,?2,?3,?4)")
	CheckformEntity save(CheckformEntity cf);

}
