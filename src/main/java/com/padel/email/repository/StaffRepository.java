package com.padel.email.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.padel.email.model.CoStaffEntity;



public interface StaffRepository  extends Repository<CoStaffEntity, Integer> {
	
	List<CoStaffEntity> findAll();
	
	CoStaffEntity findByStaffid (String code);
	CoStaffEntity findByEmail (String code);

}
