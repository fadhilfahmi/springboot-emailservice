package com.padel.email.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.padel.email.model.CheckmasterEntity;
import com.padel.email.model.CoStaffEntity;


public interface CheckRepository  extends Repository<CheckmasterEntity, Integer> {

	//@Query("SELECT p, r FROM LeaveMasterEntity p, CoStaffEntity r WHERE p.staffId = r.staffid AND p.staffId <> ?1 AND  p.staffId = ?2")
	//value = "SELECT * FROM leave_master l, co_staff c WHERE l.staffID = c.staffID AND l.staffID <> ?1 AND l.staffID IN ('P0703', 'P0633')", 
	//nativeQuery = true)
	//@Query(value = "SELECT * FROM USERS u WHERE u.status = 1", 
		  //nativeQuery = true)
	@Query("SELECT p FROM  CoStaffEntity p WHERE p.email <> '' AND (p.location = 'IBU PEJABAT' OR p.location = 'CAWANGAN INDERA MAHKOTA') AND p.position <> 'PENYELIA'")
    List<CoStaffEntity> getAllHQStaff();
	
	@Query("SELECT p FROM  CheckmasterEntity p WHERE p.staffId = ?1 AND p.date = ?2")
    CheckmasterEntity getCheckInfo(String staffId, String date);
	
	@Query("SELECT r.personCode FROM PersonEntity r WHERE r.id = ?1")
	String getStaffID(int id);
	
	@Transactional
	@Modifying
	@Query("update CheckmasterEntity u set u.temperature = ?1, u.status = ?2, u.timevalid = ?3 where u.date = ?4 and u.staffId = ?5")
	void update(double temp, String status,String time, String date, String staffId);
	
	CheckmasterEntity save(CheckmasterEntity cm);
	
//	@Modifying
//	@Query("INSERT INTO CheckmasterEntity (id, staffId, date, timescan, timevalid, temperature, status) VALUES (?1,?2,?3,?4,?5,?6,?7)")
//	String saveForVIP(String id, String staffId, String date, String timescan, String timevalid, double temp, String status);
	
	
	@Query(
			value = "SELECT ifnull(concat(lpad(max(id)+1,10,'0')), '0000000001') as new from checkmaster ", 
			  nativeQuery = true)
    String getMaxID();
	
	/*
	 * @Modifying
	 * 
	 * @Query("update User u set u.firstname = ?1, u.lastname = ?2 where u.id = ?3")
	 * void setUserInfoById(String firstname, String lastname, Integer userId);
	 */
}
