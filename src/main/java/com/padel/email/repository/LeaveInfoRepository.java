package com.padel.email.repository;

import org.springframework.data.repository.Repository;

import com.padel.email.model.LeaveInfoEntity;

public interface LeaveInfoRepository  extends Repository<LeaveInfoEntity, Integer> {
	
	LeaveInfoEntity findByStaffId (String code);
	
	LeaveInfoEntity findByStaffIdAndYear (String code, String year);

}
