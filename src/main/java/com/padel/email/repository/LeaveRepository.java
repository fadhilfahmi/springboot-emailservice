package com.padel.email.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.padel.email.model.LeaveMasterEntity;

public interface LeaveRepository  extends Repository<LeaveMasterEntity, Integer> {

List<LeaveMasterEntity> findAll();
	
	LeaveMasterEntity findByLeaveId (String code);
	
	//LeaveMasterEntity findByStaffId (String code);
	
	List<LeaveMasterEntity> findByStaffId (String code);
	
	@Query("SELECT p FROM LeaveMasterEntity p")
    List<LeaveMasterEntity> findByYearandPeriod();
	
	@Query("SELECT p FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.status <> 'Approved'")
    List<LeaveMasterEntity> findLeaveNotApproved(String staffId);
	
	@Query("SELECT p FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.datestart >= CURDATE()")
    List<LeaveMasterEntity> getNewLeave(String staffId);
	
	@Query("SELECT count(p) FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.datestart >= CURDATE()")
    int getCountLeave(String staffId);
	
	//"SELECT sum(days) as cnt FROM leave_master WHERE staffID=? and year = ? and status = ? and type <> ?"
	@Query("SELECT sum(p.days) FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.year =  ?2 AND p.status = 'Approved' AND p.type <> 'Cuti Sakit'")
    double getTotalLeaveOfTheYear(String staffId, String year);
	
	@Query("SELECT sum(p.days) FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.year =  ?2 AND p.status = 'Approved' AND p.type = 'Cuti Sakit'")
    double getTotalSickLeaveUseOfTheYear(String staffId, String year);
	
	@Query("SELECT p, r FROM LeaveMasterEntity p, CoStaffEntity r WHERE p.staffId = r.staffid AND p.staffId <> ?1 AND  p.staffId = ?2")
    List<LeaveMasterEntity> getAllLeaveExcludeYou(String staffId, String superViseeID);
	
	@Query("SELECT p FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.status = ?2")
    List<LeaveMasterEntity> getLeaveWithStatus(String staffId, String status);
	
	@Query("SELECT p FROM LeaveMasterEntity p WHERE p.status NOT IN ('Approved','Rejected')")
    List<LeaveMasterEntity> getUnapprovedLeave();
	

	//value = "SELECT * FROM leave_master l, co_staff c WHERE l.staffID = c.staffID AND l.staffID <> ?1 AND l.staffID IN ('P0703', 'P0633')", 
	//nativeQuery = true)
	
	
	
	//@Query("SELECT p FROM SecModuleEntity p WHERE LENGTH(RTRIM(p.moduleid)) = ?1 AND p.moduleid LIKE ?2%")
    //List<SecModuleEntity> findSecModuleEntitiesByModuleid(int length, String moduleid);
	
}
