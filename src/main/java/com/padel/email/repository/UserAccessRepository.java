package com.padel.email.repository;

import org.springframework.data.repository.Repository;

import com.padel.email.model.UserAccessEntity;


public interface UserAccessRepository  extends Repository<UserAccessEntity, Integer> {
	
	UserAccessEntity findByStaffId (String code);

}
