package com.padel.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.impossibl.postgres.api.jdbc.PGConnection;
import com.impossibl.postgres.api.jdbc.PGNotificationListener;
import com.impossibl.postgres.jdbc.PGDataSource;
import com.padel.email.model.CheckformEntity;
import com.padel.email.model.CheckmasterEntity;
import com.padel.email.model.FaceResult;
import com.padel.email.repository.CheckRepository;
import com.padel.email.service.CheckInfoService;
import com.padel.email.service.CheckService;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Component
public class Listener {

	private static final Logger log = LoggerFactory.getLogger(Listener.class);
	// Create the queue that will be shared by the producer and consumer


	// Database connection
	static PGConnection connection;
	
	@Autowired
	private CheckService checkService;
	
	@Autowired
	private CheckInfoService checkInfoService;
	
	//static FaceResult datas = new FaceResult();

	@EventListener(ApplicationReadyEvent.class)
	public void ListenNotify() throws InterruptedException {
		
		PGNotificationListener listener = new PGNotificationListener() {
			
			
			@Override
			public void notification(int processId, String channelName, String payload) {
				// Add event and payload to the queue
				//queue.add("/channels/" + channelName + " " + payload);
				 //System.out.println(payload);
				 
				 ObjectMapper objectMapper = new ObjectMapper();

			        //read json file and convert to customer object
			        try {
			        	FaceResult datas = objectMapper.readValue(payload, FaceResult.class);
						//System.out.println(datas.getData().getTemp_data());
						double newtemp = Double.parseDouble(datas.getData().getTemp_data());
						//UpdateeDaftar.updatedata(datas);
						//if(datas != null) {
							//log.info("StaffID :  {}", checkService.getStaffID(37));
						//}
						//log.info("StaffID :  {}", checkService.getStaffID(datas.getData().getPerson_id()));
						
						
						
						String date = datas.getData().getSwip_card_rev_time().substring(0, 10);
						String time = datas.getData().getSwip_card_rev_time().substring(11, 19);
						String staffID = String.valueOf(checkService.getStaffID(datas.getData().getPerson_id()));
						
						String status = "Gagal";
						
						CheckmasterEntity cm = checkService.getCheckInfo(staffID, date);
						
						if(cm != null) {
							CheckformEntity cf = checkInfoService.getCheckFormInfo(cm.getId());
							
							//suhu indicator
							if(cf.getQuestion1().equals("") && cf.getQuestion2().equals("Tidak") && cf.getQuestion3().equals("Tidak")) {
								status = "Lulus";
							}
							
							if(newtemp > 37.4) {
								status = "Gagal";
							}
							//--------------
							
							//log.info("Date :  {}", time);
									
							if(cm.getStatus().equals("Pending") || cm.getStatus().equals("Gagal")) {//boleh test lebih dari sekali kalau temp tinggi
								checkService.update(newtemp, status, time, date,  staffID);
								System.out.println("Updated : "+staffID+"-"+newtemp+"-"+date+"-"+time+"-"+status);
							}else {
								System.out.println("Scanned & Approved! : "+staffID+"-"+date+"-"+time);
							}
							
						}else {
							
							if(staffID.equals("P0832") || staffID.equals("P0004")) {
								String maxID = checkService.getMaxID();
								status = "Lulus";
								CheckmasterEntity ci = new CheckmasterEntity();
								ci.setDate(date);
								ci.setId(maxID);
								ci.setStaffId(staffID);
								ci.setStatus(status);
								ci.setTemperature(newtemp);
								ci.setTimescan(time);
								ci.setTimevalid(time);
								ci.setTimeout("00:00:00");
								checkService.create(ci);
								
								CheckformEntity cf = new CheckformEntity();
								cf.setCheckId(maxID);
								cf.setQuestion1("");
								cf.setQuestion2("Tidak");
								cf.setQuestion3("Tidak");
								checkInfoService.create(cf);
								
								System.out.println("Inserted : "+staffID+"-"+newtemp+"-"+date+"-"+time+"-"+status);
								//String checkId = checkService.saveForVIP("", staffID, date, time, time, newtemp, "Lulus");
								//checkInfoService.saveQuestionForVIP(checkId, "", "Tidak", "Tidak");
							}else {
								System.out.println("Please scan QR first! - StaffID : "+staffID);
							}
							
						}
						
						
						
						
						//log.info("Temperature :  {}", newtemp);
					} catch (JsonProcessingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		};
		
		
		

		try {
			// Create a data source for logging into the db
			PGDataSource dataSource = new PGDataSource();
			dataSource.setHost("localhost");
			dataSource.setPort(5432);
			dataSource.setDatabaseName("vsm");
			dataSource.setUser("lcsb");
			dataSource.setPassword("adminlcsb");

			// Log into the db
			connection = (PGConnection) dataSource.getConnection();

			// add the callback listener created earlier to the connection
			connection.addNotificationListener(listener);

			// Tell Postgres to send NOTIFY q_event to our connection and listener
			Statement statement = connection.createStatement();
			statement.execute("LISTEN q_temp");
			statement.close();
			
			 while (true) {
	                Thread.sleep(500);
	            }

			//log.info("con :  {}", connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
